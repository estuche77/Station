package org.sip.estuche.station_sip_phone;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class CallActivity extends AppCompatActivity {

    private ArrayList<String> contactos = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call);

        ListView songsListView = findViewById(R.id.listaCont);

        String contacto = "Jake Herrera";
        contactos.add(contacto);

        contacto = "Greivin Guevara";
        contactos.add(contacto);

        contacto = "Jocelyn Román";
        contactos.add(contacto);

        contacto = "Esteban Santamaría";
        contactos.add(contacto);

        contacto = "Jason Latouche";
        contactos.add(contacto);

        songsListView.setAdapter(new ContactosAdapter());
    }

    public class ContactosAdapter extends BaseAdapter {

        public ContactosAdapter() {
            super();
        }

        @Override
        public int getCount() {
            return contactos.size();
        }

        @Override
        public Object getItem(int i) {
            return contactos.get(i);
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            LayoutInflater inflater = getLayoutInflater();
            if (view == null) {
                view = inflater.inflate(R.layout.contacto, null);
            }

            TextView contact = view.findViewById(R.id.contactoText);

            contact.setText(String.valueOf(contactos.get(i)));

            return view;
        }
    }
}
