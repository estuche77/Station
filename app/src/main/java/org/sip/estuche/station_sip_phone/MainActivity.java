package org.sip.estuche.station_sip_phone;

import android.content.Context;
import android.content.Intent;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.google.zxing.Result;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class MainActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler{
    private ZXingScannerView mScannerView;
    private boolean pause = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (pause) {
            mScannerView.stopCamera();
        }
    }

    public void onLoginClicked(View v) {
        pause = true;
        mScannerView = new ZXingScannerView(this);
        setContentView(mScannerView);
        mScannerView.setResultHandler(this);
        CameraManager manager = (CameraManager) this.getSystemService(Context.CAMERA_SERVICE);
        String id = getFrontFacingCameraId(manager);
        mScannerView.startCamera(Integer.valueOf(id));
    }

    public void onAlarmPressed(View v) {
        pause = false;
        Intent intent = new Intent(this, CallingEmergencyActivity.class);
        startActivity(intent);
    }

    @Override
    public void handleResult(Result result) {
        Intent intent = new Intent(this, CallActivity.class);
        startActivity(intent);
    }

    private String getFrontFacingCameraId(CameraManager cManager){
        try {
            for(final String cameraId : cManager.getCameraIdList()){
                CameraCharacteristics characteristics = cManager.getCameraCharacteristics(cameraId);
                int cOrientation = characteristics.get(CameraCharacteristics.LENS_FACING);
                if(cOrientation == CameraCharacteristics.LENS_FACING_FRONT) return cameraId;
            }
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
        return null;
    }
}
